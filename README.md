

 <img style="width: 180px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/行走的人小.jpg">

> ##### 视频集锦：[论文精讲与重现](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc) | [实证研究设计](https://mp.weixin.qq.com/s/NGwsr92_Vr1DGRbVqDVQIA) | [空间计量](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e) | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e) | [直击面板数据模型-Free](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)
> ##### 导航： 📍 [连享会主页](https://www.lianxh.cn)  | 📍 [知乎专栏](https://www.zhihu.com/people/arlionn/) | 📍 [直播课](http://lianxh.duanshu.com) 


&emsp;



**MHE！MHE！** 这是一本写给实证分析前线勇士的书。  

「[_Mostly Harmless Econometrics_](http://www.mostlyharmlesseconometrics.com/)；安格里斯特, 皮施克. 基本无害的计量经济学: 实证研究者指南 / (美) 安格里斯特, (美) 皮施克著 ; 郎金焕, 李井奎译.[M]. 2012.」一书各章的实现过程。

对照一下同样的任务同时用四种不同的软件或语言来写，是学习 Python 和 Stata 的绝佳资料。

### 书评

> “Finally – An econometrics book for practitioners! Not only for students, Mostly Harmless Econometricsis a fantastic resource for anyone who does empirical work.”       
— Sandra Black, UCLA

> “This is a remarkable book–it does the profession a great service by taking knowledge that is usually acquired over many years and distilling it in such a succinct manner.”        
— Amitabh Chandra, Harvard Kennedy School of Government

> “MHE is a fantastic book that should be read cover-to-cover by any young applied micro economist.  The book provides an excellent mix of statistical detail, econometric intuition and practical instruction.  The topic coverage includes the bulk of econometric tools used in the vast majority of applied microeconomics.  I wish there was an econometric textbook this well done when I was in graduate school.”     
— Bill Evans, University of Notre Dame


&emsp;

----

# Mostly Harmless Replication


![](https://images.gitee.com/uploads/images/2020/0223/104540_9351cbce_1522177.png)

## Synopsis

A bold attempt to replicate the tables and figures from the book [_Mostly Harmless Econometrics_](http://www.mostlyharmlesseconometrics.com/) in the following languages:
* Stata
* R
* Python
* Julia

## Chapters
1. Questions about _Questions_
2. The Experimental Ideal
3. [Making Regression Make Sense](03%20Making%20Regression%20Make%20Sense/03%20Making%20Regression%20Make%20Sense.md)
4. [Instrumental Variables in Action](04%20Instrumental%20Variables%20in%20Action/04%20Instrumental%20Variables%20in%20Action.md)
5. [Parallel Worlds](05%20Fixed%20Effects%2C%20DD%20and%20Panel%20Data/05%20Fixed%20Effects%2C%20DD%20and%20Panel%20Data.md)
6. [Getting a Little Jumpy](06%20Getting%20a%20Little%20Jumpy/06%20Getting%20a%20Little%20Jumpy.md)
7. [Quantile Regression](07%20Quantile%20Regression/07%20Quantile%20Regression.md)
8. [Nonstandard Standard Error Issues](08%20Nonstandard%20Standard%20Error%20Issues/08%20Nonstanard%20Standard%20Error%20Issues.md)

## Getting started
Check out [Getting Started](https://github.com/vikjam/mostly-harmless-replication/wiki/Getting-started) in the Wiki for tips on setting up your machine with each of these languages.

## Contributions
Feel free to submit [pull requests](https://github.com/blog/1943-how-to-write-the-perfect-pull-request)!

&emsp; 
&emsp; 

&emsp;

## 相关课程

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  
> <img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">

---
### 课程一览   


> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[Stata暑期班](https://gitee.com/arlionn/PX)** | 连玉君 <br> 江艇| [线上直播 9 天](https://www.lianxh.cn/news/e87cdb5c116d0.html) <br> 2020.7.28-8.7 |
| **[效率分析-专题](https://gitee.com/arlionn/TE)** | 连玉君<br>鲁晓东<br>张&emsp;宁 | [视频-TFP-SFA-DEA](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线，3天 |
| **文本分析/爬虫** | 游万海<br>司继春 | [视频-文本分析与爬虫](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线，4天 |
| **[空间计量系列](https://lianxh.duanshu.com/#/brief/course/958fd224da8548e1ba7ff0740b536143)** | 范巧    | [空间全局模型](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), [空间权重矩阵](https://lianxh.duanshu.com/#/brief/course/94a5361647384a18852d28d1b9246362) <br> [空间动态面板](https://lianxh.duanshu.com/#/brief/course/f4e4b6b1e77c4ff88cecb685bbde07c3), [空间DID](https://lianxh.duanshu.com/#/brief/course/ff7dc9e0b82b40dab2047af0d01e96d0) |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
|     |     | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |


> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

 <img style="width: 180px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/行走的人小.jpg">

&emsp;

> #### [连享会 - Stata 暑期班](https://gitee.com/arlionn/PX)     
> **线上直播 9 天**：2020.7.28-8.7  
> **主讲嘉宾**：连玉君 (中山大学) | 江艇 (中国人民大学)      
> **课程主页**：<https://gitee.com/arlionn/PX> | [微信版](https://mp.weixin.qq.com/s/_ypP4ol1_VnjOOBGZeAnoQ)  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/2020Stata暑期班海报竖版CL550.png "连享会：Stata暑期班，9天直播")

&emsp; 


&emsp;

> #### [连享会 - 文本分析与爬虫 - 专题视频](https://www.lianxh.cn/news/88426b2faeea8.html)    
> 主讲嘉宾：司继春 || 游万海

![连享会-文本分析与爬虫-专题视频教程](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lanNew-文本分析-海报002.png "连享会-文本分析与爬虫-专题视频，四天课程，随随时学")

&emsp;

> #### [连享会 - 效率分析专题](https://www.lianxh.cn/news/a94e5f6b8df01.html)     
> **已上线**：可随时购买学习+全套课件，[课程主页](https://gitee.com/arlionn/TE) 已经放置板书和 FAQs   
> 主讲嘉宾：连玉君 | 鲁晓东 | 张宁      
> [课程主页](https://gitee.com/arlionn/TE)，[微信版](https://mp.weixin.qq.com/s/zRotMGebIQqaMih8B71fdg) &ensp; <https://gitee.com/arlionn/TE>

![连享会-效率分析专题视频](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TE专题_连享会_lianxh.cn700.png "连享会-效率分析专题视频，三天课程，随时学")

&emsp; 


&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，300+ 推文，实证分析不再抓狂。
- **公众号推文分类：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：
  - `课程, 直播, 视频, 客服, 模型设定, 研究设计, 暑期班`
  - `stata, plus，Profile, 手册, SJ, 外部命令, profile, mata, 绘图, 编程, 数据, 可视化`
  - `DID，RDD, PSM，IV，DID, DDD, 合成控制法，内生性, 事件研究`, `交乘, 平方项, 缺失值, 离群值, 缩尾, R2, 乱码, 结果`
  - `Probit, Logit, tobit, MLE, GMM, DEA, Bootstrap, bs, MC, TFP`, `面板, 直击面板数据, 动态面板, VAR, 生存分析, 分位数`
  - `空间, 空间计量, 连老师, 直播, 爬虫, 文本, 正则, python`
  - `Markdown, Markdown幻灯片, marp, 工具, 软件, Sai2, gInk, Annotator, 手写批注`, `盈余管理, 特斯拉, 甲壳虫, 论文重现`, `易懂教程, 码云, 教程, 知乎`


---

![连享会主页  lianxh.cn](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会跑起来就有风400.png "连享会主页：lianxh.cn")



--- - --

> 连享会小程序：扫一扫，看推文，看视频……

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)

---

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)



> &#x270F;  连享会学习群-常见问题解答汇总：  
> &#x2728;  <https://gitee.com/arlionn/WD>  

